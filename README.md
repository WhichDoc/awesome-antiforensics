# Awesome Anti-Forensics

A collection of tools and methodologies used to disrupt artifact collection. 

- [Awesome Anti-Forensics](#awesome-antiforensics)
  - [Network](#network)
    - [Traffic Restriction](#traffic-restriction)
    - [Traffic Encryption](#traffic-encryption)
  - [Software](#software) 
    - [Operating Systems](#operating-systems)
    - [BIOS](#bios)
    - [Browsers](#browsers)
    - [Communications](#communications)
  - [Cryptography](#cryptography)
    - [Cryptographic Software](#cryptographic-software)
    - [Cryptocurrency](#cryptocurrency)
  - [Obscurity](#obscurity) 
	 - [Steganography](#steganography)
  - [Artifact Removal](#artifact-removal)
    - [File Deletion](#file-deletion)
    - [System Cleaners](#system-cleaners)
    - [Metadata Removal](#metadata-removal)
  - [Hardware](#hardware)
    - [Airgap](#airgap)
    - [Killswitch](#killswitch)
    - [EMF Shielding](#emf-shielding)
  - [Operating Systems](#operating-systems)
  - [EBooks](#ebooks)
  - [Other Awesome Lists](#other-awesome-lists)
    - [Other Security Awesome Lists](#other-security-awesome-lists)
    - [Other Common Awesome Lists](#other-common-awesome-lists)
  - [Contributing](#contributing)
------

## Network
### Traffic Restriction
#### Firewall
- [pfSense](https://www.pfsense.org/) - Firewall and Router FreeBSD distribution.
- [OPNsense](https://opnsense.org/) - is an open source, easy-to-use and easy-to-build FreeBSD based firewall and routing platform. OPNsense includes most of the features available in expensive commercial firewalls, and more in many cases. It brings the rich feature set of commercial offerings with the benefits of open and verifiable sources.
- [OpenWRT](https://openwrt.org/)
- [DD-WRT](https://dd-wrt.com)

#### Proxy

#### Bridge 

### Traffic Encryption/Routing
- [ProtonVPN](https://protonvpn.com)
- [Mullvad](https://mullvad.net)
- [CryptoStorm](https://www.cryptostorm.is)
- [OpenVPN](https://openvpn.net) - OpenVPN is an open source software application that implements virtual private network (VPN) techniques for creating secure point-to-point or site-to-site connections in routed or bridged configurations and remote access facilities. It uses a custom security protocol that utilizes SSL/TLS for key exchange.
- [Wireguard](https://www.wireguard.com) - 
- [TOR](https://torproject.org/) - The Onion Router 
- [Orbot]() - TOR proxy software for Android OS
- [I2P](https://geti2p.net/en/) - I do not recommend.
- [SOCKS5]() - 
- [nym](https://nymtech.net/) - Open-ended anonymous overlay network that works to irreversibly disguise patterns in internet traffic
#### Scripts (WARNING: Transparent proxies on the system are not full proof nor can guarantee prevention of traffic leakage)
- [torctl](https://github.com/BlackArch/torctl) - Script to redirect all traffic through tor network including dns queries for anonymizing entire system
- [anonsurf](https://github.com/ParrotSec/anonsurf) - Transparent proxy to route all traffic via TOR
- [kalitorify](https://github.com/brainfucksec/kalitorify) - Transparent proxy utilizing iptables to route all traffic via TOR


## Containerization
- [QEMU](https://www.qemu.org) - Machine Emulator
- [VirtualBox](https://www.virtualbox.org) - Hypervisor
- [HiddenVM](https://github.com/aforensics/HiddenVM) - Run VirtualBox binaries inside a hidden Veracrypt volume on an anti-forensic OS 



## Logging Mechanisms
- Disable logging daemons on GNU/Linux (systemD service manager)
  - `systemctl disable syslog.service`
  - `systemctl disable rsyslog.service`
  - `systemctl disable systemd-journald.service`


## Software
### Operating Systems 
#### Desktop
- [TAILS](https://tails.boum.org/) - The Amnesiac Incognito Live System 
- [Whonix](https://www.whonix.org/) - Privacy and Security focused Linux distribution | Utilizes two virtualized systems, the Workstation and Gateway, to force all traffic through TOR and prevent leakage. 
- [KickSecure](https://www.whonix.org/wiki/Kicksecure/) - Non-anonymous security baseline for Whonix. This OS can be natively installed on your device or as a virtual machine. 
- [QubesOS](https://www.qubes-os.org) - Xen-Hypervisor
- [ParrotSec](https://www.parrotsec.org/) - GNU/Linux distribution primarily for forensics & pentesting
- [HardenedBSD](https://hardenedbsd.org/) - FreeBSD with system hardening
- [Gentoo](https://www.gentoo.org/) - Customizable minimal base OS.
- [Arch](https://archlinux.org) - Highly customizable minimal base OS with large community support. Consider variants of Arch such as [Artix](https://artixlinux.org) which are not reliant on the systemD service manager.
#### Mobile
- [Ubuntu Touch](https://ubuntu-touch.io/)
- [GrapheneOS](https://grapheneos.org/)
- [CalyxOS](https://calyxos.org/)
- [LineageOS](https://www.lineageos.org/) 

### BIOS
- [CoreBoot](https://www.coreboot.org)  
- [Libreboot](https://libreboot.org) 

### Browsers
- [Ungoogled Chromium](https://ungoogled-software.github.io/ungoogled-chromium-binaries/) - Ungoogled variant of Chromium
- [Bromite](https://www.bromite.org/) - Ungoogled Chromium w/ added hardening and ad-blocking features for Android
- [TOR Browser](https://www.torproject.org) - Hardened firefox variant with TOR routing
- [SecBrowser](https://www.whonix.org/wiki/SecBrowser) - Security-hardened, Non-anonymous browser: TOR Browser w/out networking  

### Communications
#### Protocols
- [XMPP](https://xmpp.org) - Open standard for messaging and presence
- [Matrix](https://matrix.org/) - Open network for secure, decentralized communication
#### Clients
- [Signal](https://signal.org)
- [Threema](https://threema.ch)
- [Briar](https://briarproject.org/) - P2P Encrypted messaging
- [XMPP Client List](https://xmpp.org/software/clients.html)
- [Element](https://element.io) - Matrix Client
### Cryptography
#### Cryptographic Software
- [Veracrypt](https://www.veracrypt.fr/code/VeraCrypt/) - Open-source cryptographic software with plausible deniability use-case
- [zuluCrypt](https://mhogomchungu.github.io/zuluCrypt/) - Open-source cryptographic software for use with LUKS encrypted volumes
- [GNU Privacy Guard](https://gnupg.org/) - Utilizes PGP for signing + encryption

#### Cryptocurrency
- [PirateChain (ARRR)](https://pirate.black) - Forced implementation of zk-SNARKs protocol and designed to remove/reduce metadata available to global passive adversaries. Sender, receiver, and amounts are all encrypted and can only be decrypted by the spending/viewing keys. 
- [Monero (XMR)](https://getmonero.com) - Cryptocurrency aimed at obscurity through the use of the RingCT protocol. 
- [Zcash (ZEC)](https://z.cash) - Initial implementation of zk-SNARKs protocol. Due to majority of the holdings not residing inside the anonset of the chain (transparent addresses), tracing becomes trivial. 

### Obscurity
#### Stenography
- [StegoSuite]() - 
- []() - 
- []() - 

### Artifact Removal
#### File Deletion
- [shred]() - Function of the secure-delete package on GNU/Linux distributions - Overwrites specified files
- [sdmem]() - Function of the secure-delete package on GNU/Linux distributions - Wipes RAM
#### System Cleaners 
- [BleachBit](https://www.bleachbit.org/) - An Open-Source System Cleaner

### Metadata Removal
- [Metadata Anonymization Toolkit](https://0xacab.org/jvoisin/mat2) - Clears the embedded EXIF data inside of various media files
- [Scrambled Exif](https://gitlab.com/juanitobananas/scrambled-exif) - Clears EXIF from media on Android OS
- [DocBleach](https://github.com/docbleach/DocBleach) - An open-source Content Disarm & Reconstruct software sanitizing Office, PDF and RTF Documents.
- [ExifCleaner](https://exifcleaner.com/) 


## Hardware

### Airgap
- Airgap - The process of airgapping is the removal of wireless transmitters. Often in compliance with letter agencies, microphones/camera are physically gutted from the device
### Killswitch
- [BUSKill](https://www.buskill.in) - Dead man's switch triggered when USB connection is severed
### Hardware RNGs

### EMF Shielding

### Physical Destruction
- Deguass
- Shred 
- Drill
- Pulping
- My personal favorite: Execution

## Automated Procedures



### Sniffer
- [wireshark](https://www.wireshark.org) - Wireshark is a free and open-source packet analyzer. It is used for network troubleshooting, analysis, software and communications protocol development, and education. Wireshark is very similar to tcpdump, but has a graphical front-end, plus some integrated sorting and filtering options.
- [netsniff-ng](http://netsniff-ng.org/) -  netsniff-ng is a free Linux networking toolkit, a Swiss army knife for your daily Linux network plumbing if you will. Its gain of performance is reached by zero-copy mechanisms, so that on packet reception and transmission the kernel does not need to copy packets from kernel space to user space and vice versa.

### Identity

#### Alias 
- [faker.js](https://cdn.rawgit.com/Marak/faker.js/master/examples/browser/index.html) - JS used for alias creation

#### Alibi
- scripts needed for this 



### Forensics

- [grr](https://github.com/google/grr) - GRR Rapid Response is an incident response framework focused on remote live forensics.
- [Volatility](https://github.com/volatilityfoundation/volatility) - Python based memory extraction and analysis framework.
- [mig](http://mig.mozilla.org/) - MIG is a platform to perform investigative surgery on remote endpoints. It enables investigators to obtain information from large numbers of systems in parallel, thus accelerating investigation of incidents and day-to-day operations security.
- [ir-rescue](https://github.com/diogo-fernan/ir-rescue) - *ir-rescue* is a Windows Batch script and a Unix Bash script to comprehensively collect host forensic data during incident response.
- [Logdissect](https://github.com/dogoncouch/logdissect) - CLI utility and Python API for analyzing log files and other data.
- [Meerkat](https://github.com/TonyPhipps/Meerkat) - PowerShell-based Windows artifact collection for threat hunting and incident response.


### Online resources

- [Security related Operating Systems @ Rawsec](http://rawsec.ml/en/security-related-os/) - Complete list of security related operating systems
- [Best Linux Penetration Testing Distributions @ CyberPunk](https://n0where.net/best-linux-penetration-testing-distributions/) - Description of main penetration testing distributions
- [Security @ Distrowatch](http://distrowatch.com/search.php?category=Security) - Website dedicated to talking about, reviewing and keeping up to date with open source operating systems
- [PrivacyTools](https://www.privacytools.io/) - Maintained list of tools to combat digital surveillance
- [IntelTechniques](https://inteltechniques.com) - OSINT & privacy related resources/services
## Books
- [The Code Book](https://simonsingh.net/books/the-code-book/) - Explores the history of cryptography and modern application
- [Real World Cryptography](https://www.manning.com/books/real-world-cryptography) - This early-access book teaches you applied cryptographic techniques to understand and apply security at every level of your systems and applications.
- [The End of Trust](https://www.eff.org/the-end-of-trust) - Collection of essays and interviews focusing on issues related to technology, privacy, and surveillance

### Other Security Awesome Lists

- [Android Security Awesome](https://github.com/ashishb/android-security-awesome) - A collection of android security related resources.
- [Awesome ARM Exploitation](https://github.com/HenryHoggard/awesome-arm-exploitation) - A curated list of ARM exploitation resources.
- [Awesome CTF](https://github.com/apsdehal/awesome-ctf) - A curated list of CTF frameworks, libraries, resources and software.
- [Awesome Cyber Skills](https://github.com/joe-shenouda/awesome-cyber-skills) - A curated list of hacking environments where you can train your cyber skills legally and safely.
- [Awesome Personal Security](https://github.com/Lissy93/personal-security-checklist) - A curated list of digital security and privacy tips, with links to further resources.
- [Awesome Hacking](https://github.com/carpedm20/awesome-hacking) - A curated list of awesome Hacking tutorials, tools and resources.
- [Awesome Honeypots](https://github.com/paralax/awesome-honeypots) - An awesome list of honeypot resources.
- [Awesome Malware Analysis](https://github.com/rshipp/awesome-malware-analysis) - A curated list of awesome malware analysis tools and resources.
- [Awesome PCAP Tools](https://github.com/caesar0301/awesome-pcaptools) - A collection of tools developed by other researchers in the Computer Science area to process network traces.
- [Awesome Pentest](https://github.com/enaqx/awesome-pentest) - A collection of awesome penetration testing resources, tools and other shiny things.
- [Awesome Linux Containers](https://github.com/Friz-zy/awesome-linux-containers) - A curated list of awesome Linux Containers frameworks, libraries and software.
- [Awesome Incident Response](https://github.com/meirwah/awesome-incident-response) - A curated list of resources for incident response.
- [Awesome Web Hacking](https://github.com/infoslack/awesome-web-hacking) - This list is for anyone wishing to learn about web application security but do not have a starting point.
- [Awesome Threat Intelligence](https://github.com/hslatman/awesome-threat-intelligence) - A curated list of threat intelligence resources.
- [Awesome Threat Modeling](https://github.com/redshiftzero/awesome-threat-modeling) - A curated list of Threat Modeling resources.
- [Awesome Pentest Cheat Sheets](https://github.com/coreb1t/awesome-pentest-cheat-sheets) - Collection of the cheat sheets useful for pentesting
- [Awesome Industrial Control System Security](https://github.com/mpesen/awesome-industrial-control-system-security) - A curated list of resources related to Industrial Control System (ICS) security.
- [Awesome YARA](https://github.com/InQuest/awesome-yara) - A curated list of awesome YARA rules, tools, and people.
- [Awesome Threat Detection and Hunting](https://github.com/0x4D31/awesome-threat-detection) - A curated list of awesome threat detection and hunting resources.
- [Awesome Container Security](https://github.com/kai5263499/container-security-awesome) - A curated list of awesome resources related to container building and runtime security
- [Awesome Crypto Papers](https://github.com/pFarb/awesome-crypto-papers) - A curated list of cryptography papers, articles, tutorials and howtos.
- [Awesome Shodan Search Queries](https://github.com/jakejarvis/awesome-shodan-queries) - A collection of interesting, funny, and depressing search queries to plug into Shodan.io.
- [Awesome Security Talks & Videos](https://github.com/PaulSec/awesome-sec-talks) - A curated list of awesome security talks, organized by year and then conference. 
